# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 18:40:36 2019

@author: Jake
"""

# Python 3 code to numerically differentiate the pressure in order to
# calculate the geostrophic wind relation using 2-point differencing,
# compare with the analytic solution and plot

import numpy as np
import matplotlib.pyplot as plt
from differentiate import gradient_2point
from differentiate import gradient_4point


def geostrophicWind():
    # Input parameters describing the problem
    import geoParameters as gp
    
    # Resolution
    N = 10                   # the number of intervals to divide space into
    dy = (gp.ymax - gp.ymin)/N # the length of spacing
    
    # The spacial dimension, y:
    y = np.linspace(gp.ymin, gp.ymax, N+1)
    
    # The geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)
    
    # The pressure at the y points
    p = gp.pressure(y)
    
    # The pressure gradient and the wind using two point differences
    # To use 4 point difference, change 'gradient_2point' to 'gradient_4point'
    # in below code.
    dpdy = gradient_4point(p, dy)
    u_2point = gp.geoWind(dpdy)
    u_diff_err = abs(uExact - u_2point)
    
    # Graph to compare the numerical and analytic solutions
    # plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)

    # Plot the approximate and exact wind at y points
    plt.figure(1)
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k--', label='Four-point differences', \
             ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCent4.pdf')
    plt.show()
    
    plt.figure(2)
    plt.plot(y/1000, u_diff_err)
    plt.xlabel('y (km)')
    plt.ylabel('absolute error values')
    plt.tight_layout()
    plt.savefig('plots/numErr4.pdf')
    plt.show()
    
    print(u_diff_err)
if __name__ == "__main__":
    geostrophicWind()
